package com.cjq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CjqEsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CjqEsApiApplication.class, args);
	}

}
