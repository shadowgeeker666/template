package com.cjq;

import com.alibaba.fastjson.JSON;
import com.cjq.domain.User;
import org.apache.lucene.util.QueryBuilder;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;

@SpringBootTest
class CjqEsApiApplicationTests {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    //测试创建索引：
    @Test
    void testCreatIndex() throws IOException {
        CreateIndexRequest request = new CreateIndexRequest("cjq_index");
        System.out.println(restHighLevelClient.indices().create(request, RequestOptions.DEFAULT));
    }


    //测试获取索引：
    @Test
    void testExistsIndex() throws IOException {
        GetIndexRequest request = new GetIndexRequest("cjq_index");
        System.out.println(restHighLevelClient.indices().get(request, RequestOptions.DEFAULT));
        System.out.println(restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT));
    }


    //测试删除索引
    @Test
    void testDeleteIndexRequest() throws IOException {
        DeleteIndexRequest request = new DeleteIndexRequest("cjq_index");
        AcknowledgedResponse resp = restHighLevelClient.indices().delete(request, RequestOptions.DEFAULT);
        System.out.println(resp.isAcknowledged());
    }


    //测试添加文档记录
    @Test
    void testAddDocument() throws IOException {
        User user = new User("cjq", 20);
        //创建请求
        IndexRequest request = new IndexRequest("geeker_chen");
        //规则
        request.id("2")
                .timeout(TimeValue.timeValueSeconds(1))
                .source(JSON.toJSONString(user), XContentType.JSON);

        //发送请求
        IndexResponse response = restHighLevelClient.index(request, RequestOptions.DEFAULT);

        System.out.println(response.toString());
        RestStatus status = response.status();
        System.out.println(status == RestStatus.OK || status == RestStatus.CREATED);
    }


    //判断某索引下文档id是否存在
    @Test
    void testIdExists() throws IOException {
        GetRequest request = new GetRequest("geeker_chen", "1");
        //不获取_source上下文 storedFields
        request.fetchSourceContext(new FetchSourceContext(false))
                .storedFields("_none_");
        // 判断此id是否存在！
        System.out.println(restHighLevelClient.exists(request, RequestOptions.DEFAULT));
    }


    //根据id获取记录
    @Test
    void testGetDocument() throws IOException {
        GetRequest request = new GetRequest("geeker_chen", "1");
        GetResponse response = restHighLevelClient.get(request, RequestOptions.DEFAULT);
        System.out.println(response.getSourceAsString());
        System.out.println(response);
    }


    //更新文档记录
    @Test
    void testUpdateDocument() throws IOException {
        UpdateRequest request = new UpdateRequest("geeker_chen", "1");
        request.timeout("1s")
                .timeout(TimeValue.timeValueSeconds(1))
                .doc(JSON.toJSONString(new User("cjq", 21)), XContentType.JSON);
        UpdateResponse response = restHighLevelClient.update(request, RequestOptions.DEFAULT);
        System.out.println(response.status());
    }


    //删除文档记录
    @Test
    void testDelete() throws IOException {
        DeleteRequest request = new DeleteRequest("geeker_chen", "2");
        DeleteResponse response = restHighLevelClient.delete(request, RequestOptions.DEFAULT);
        System.out.println(response.status());
    }


    //批量添加文档
    @Test
    void testBulkRequest() throws IOException {
        BulkRequest bulkRequest = new BulkRequest();
        //timeout
        bulkRequest.timeout(TimeValue.timeValueSeconds(2));
        ArrayList<User> users = new ArrayList<>();
        users.add(new User("cjq1", 1));
        users.add(new User("cjq2", 2));
        users.add(new User("cjq3", 3));
        users.add(new User("cjq4", 4));
        users.add(new User("cjq5", 5));
        users.add(new User("cjq6", 6));

        for (int i = 0; i < users.size(); i++) {
            bulkRequest
                    .add(new IndexRequest("cjq666_index")
                            .id("" + (i + 1))
                            .source(JSON.toJSONString(users.get(i)), XContentType.JSON));
        }

        //bulk
        BulkResponse response = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        System.out.println(response.status());
        System.out.println(response.hasFailures());
    }

    //查询测试
    /*
    *使用QueryBuilder
    * termQuery("key", obj) 完全匹配
    * termsQuery("key", obj1, obj2..)  一次匹配多个值
    *matchQuery("key", Obj) 单个匹配, field不支持通配符, 前缀具高级特性
    * multiMatchQuery("text", "field1", "field2"..); 匹配多个字段, field有通配符特性
    * matchAllQuery();     匹配所有文件
    * */

    @Test
    void testSearch() throws IOException {
        SearchRequest searchRequest = new SearchRequest("cjq666_index");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();
        sourceBuilder.query(matchAllQueryBuilder)
                .timeout(TimeValue.timeValueSeconds(1));

        searchRequest.source(sourceBuilder);
        SearchResponse resp = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        resp.getHits().forEach(
                it->{
                    System.out.println(it.getSourceAsMap());
//                    System.out.println(it.getSourceAsString());
                }
        );

    }
}
